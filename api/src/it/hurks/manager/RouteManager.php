<?php

namespace it\hurks\manager;

use Phalcon\Mvc\Micro;
use Phalcon\Di\FactoryDefault;

/**
 * This manager class is responsible for
 * all route related functionality.
 *
 * @author Hurks IT
 */
final class RouteManager {

    /**
     * @var Micro
     */
    private $phalcon;

    /**
     * @return Micro
     */
    public function getPhalcon()
    {
        return $this->phalcon;
    }

    public function __construct () {
        $this->initializePhalconMicroFramework();
    }

    /**
     * Initialize all routes
     */
    public function initializeRoutes () {
        require_once "routes.php";
    }

    /**
     * Initializes the phalcon micro framework
     */
    private function initializePhalconMicroFramework () {
        $this->phalcon = new Micro(new FactoryDefault());
    }

}