<?php

namespace it\hurks\manager;

/**
 * This file manages all HTTP related functionality.
 *
 * @author Hurks IT
 */
final class HttpManager {

    /**
     * @var object
     */
    private $requestBody;

    public function __construct(){
    }

    /**
     * Outputs a response as JSON.
     * @param $data null the data to return
     * @param $httpResponseCode int the http response code to return
     * @param $feedback string the feedback for the end-user.
     */
    public function outputJson ($data, $httpResponseCode = 200, $feedback = "") {
        ob_end_clean();

        http_response_code($httpResponseCode);
        header('Content-Type: application/json');

        die(json_encode((object)[
            "data" => $data,
            "feedback" => $feedback
        ]));
    }

    /**
     * Get the request body
     * @return object
     */
    public function getRequestBody()
    {
        $this->requestBody = ($this->requestBody ?: json_decode(file_get_contents("php://input")) ?: (object)[]);
        return $this->requestBody;
    }

}