<?php

namespace it\hurks\controller;

use it\hurks\core\App;

/**
 * The controller for all FTP related end-points.
 *
 * @author Hurks IT
 */
final class FTPController {

    const BACKUP_FOLDER_LOCATION = "backup/";

    public function __construct ()
    {
    }

    /**
     * Lists all backed up FTP files
     */
    public function listProcessedFiles () {
        $files = [];

        foreach (scandir("backup") as $fileName) {
            if (in_array($fileName, [".gitkeep", "./", "../", ".", ".."])) {
                continue;
            }

            $files[] = $fileName;
        }

        App::getHttpManager()->outputJson($files, 200, "Successfully listed files.");
    }

    /**
     * Get a certain processed file.
     * @param $fileName string the name of the file
     */
    public function detailProcessedFile ($fileName) {
        if (!file_exists("backup/$fileName") || $fileName === "") {
            App::getHttpManager()->outputJson(null, 404, "File does not exist.");
        }

        $file = file_get_contents("backup/$fileName");
        App::getHttpManager()->outputJson((object)[
            "fileName" => $fileName,
            "time" => date("d-m-Y H:i:s", trim(substr($fileName, 0, stripos($fileName, "-")))),
            "amtOfLines" => count(explode("\n", $file) ?: [])
        ], 200, "File found.");
    }

    /**
     * Parse a certain file from the given FTP server to the back-up folder.
     */
    public function parseFile () {
        $requestBody = App::getHttpManager()->getRequestBody();
        if (!isset($requestBody->fileName) || !is_string($requestBody->fileName) || $requestBody->fileName == "") {
            App::getHttpManager()->outputJson(null, 400, "Please provide a valid file name.");
        }

        $this->validateRequestBodyForFtpDetailsOrDie(App::getHttpManager()->getRequestBody());
        $ftpResource = $this->connectToFTPServerOrDie();
        $time = time();

        ob_end_clean();
        ob_start();

        if (ftp_get($ftpResource, "php://output", $requestBody->fileName, FTP_BINARY)) {
            file_put_contents("backup/" . str_ireplace("/", "_", "$time - $requestBody->fileName"), ob_get_contents());

            App::getHttpManager()->outputJson((object)[
                "fileName" => "$time - $requestBody->fileName",
                "amtOfLines" => count(explode("\n", ob_get_contents()) ?: []),
                "time" => date("d-m-Y H:i:s", trim(substr("$time - $requestBody->fileName", 0, stripos("$time - $requestBody->fileName", "-"))))
            ], 200, "Successfully found file.");
        }

        App::getHttpManager()->outputJson(null, 400, "Could not download file from server.");
    }

    /**
     * Connect to the FTP server provided in the request body
     * or dies.
     */
    private function connectToFTPServerOrDie () {
        $requestBody = App::getHttpManager()->getRequestBody();

        if (!is_resource($ftpResource = ftp_connect($requestBody->hostname, $requestBody->port, 1000))) {
            App::getHttpManager()->outputJson(null, 400, "Could not create ftp resource.");
        }

        if (!ftp_login($ftpResource, $requestBody->username, $requestBody->password)) {
            App::getHttpManager()->outputJson(null, 400, "Could not logon to ftp resource.");
        }

        return $ftpResource;
    }

    /**
     * Checks if the request body contains valid ftp details
     * and will die with json information if it is not valid.
     * @param $requestBody
     */
    private function validateRequestBodyForFtpDetailsOrDie(&$requestBody)
    {
        foreach (["hostname", "username", "password", "port"] as $propertyName) {
            if (!isset($requestBody->$propertyName)) {
                App::getHttpManager()->outputJson(null, 400, "Please fill in a {$propertyName}.");
            }

            if ($propertyName === "port" && !is_integer($requestBody->port)) {
                App::getHttpManager()->outputJson(null, 400, "Please fill in a valid port number.");
            }

            if ($propertyName !== "port" && (!is_string($requestBody->$propertyName) || strlen($requestBody->$propertyName) < 1)) {
                App::getHttpManager()->outputJson(null, 400, "Please fill in a valid {$propertyName}.");
            }
        }
    }

}