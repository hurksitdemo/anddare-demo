<?php

namespace it\hurks\core;

use it\hurks\manager\HttpManager;
use it\hurks\manager\RouteManager;

/**
 * The starting class for this PHP environment.
 *
 * @author Hurks IT
 */
final class App {

    /**
     * @var RouteManager
     */
    private static $routeManager;

    /**
     * @var HttpManager
     */
    private static $httpManager;

    /**
     * @return RouteManager
     */
    public static function getRouteManager()
    {
        return self::$routeManager;
    }

    /**
     * @return HttpManager
     */
    public static function getHttpManager()
    {
        return self::$httpManager;
    }

    /**
     * Initialize the application
     */
    public static function initialize() {
        require_once "AutoLoader.php";
        AutoLoader::initialize();

        self::initializeProperties();

        self::$routeManager->initializeRoutes();
    }

    /**
     * Run the application
     */
    public static function run () {
        self::$routeManager->getPhalcon()->handle();
    }

    /**
     * Initialize all property values for this class
     * after initialization.
     */
    private static function initializeProperties () {
        self::$routeManager = new RouteManager();
        self::$httpManager = new HttpManager();
    }

}