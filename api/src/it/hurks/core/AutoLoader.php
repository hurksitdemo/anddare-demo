<?php

namespace it\hurks\core;

/**
 * This class is responsible for auto-loading
 * all php classes correctly into the application.
 *
 * @author Hurks IT
 */
final class AutoLoader {

    /**
     * Initialize the auto-loader
     */
    public static function initialize () {
        spl_autoload_register(function ($className) {
            if (stripos($className, "it\\hurks") !== false) {
                require_once "src\\$className.php";
            }
        });
    }

}