<?php
use it\hurks\core\App;
$phalcon = App::getRouteManager()->getPhalcon();

$ftpController = new \it\hurks\controller\FTPController();

$phalcon->get("/ftp/processed/{fileName}", function ($fileName) use ($ftpController) {
    $ftpController->detailProcessedFile($fileName);
});

$phalcon->get("/ftp/processed", function () use ($ftpController) {
    $ftpController->listProcessedFiles();
});

$phalcon->post("/ftp", function () use ($ftpController) {
    $ftpController->parseFile();
});

$phalcon->notFound(function () {
    App::getHttpManager()->outputJson(null, 404, "The route could not be found.");
});