<?php
    session_start();

    require "src/it/hurks/core/App.php";

    use it\hurks\core\App;

    $app = new App();

    $app->initialize();

    $app->run();