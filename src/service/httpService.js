(function () {
    "use strict";
    app.service("$httpService", function ($http) {
        var self = this;

        /**
         * request - Perform an http request
         *
         * @param  {string} route               The route to call
         * @param  {string} method              The request method
         * @param  {Function} callback          The method to call after the request
         * @param  {Object} data                The request body
         * @return {Void}                       Nothing will be returned
         */
        this.request = function (route, method, callback, data) {
            $http({
                method: method,
                url: route,
                data: data || null
            })
            .then(
            function (response) {
                return callback(response);
            },
            function errorCallback(response) {
                return callback(response);
            });
        };

        /**
         * validate - Check if the request body isn't null or empty
         *
         * @param  {Object}   response the response
         * @return {boolean}           true if validated
         */
        this.validate = function (response) {
            return      response != null
                    &&  response.data != null
                    &&  response.data.body != null;
        };

        /**
         * get - Perform a get api-call
         *
         * @param  {string}     route            the route
         * @param  {Function}   successCallback  when the response was OK
         * @return {Void}
         */
        this.get = function (route, successCallback) {
            return self.request(route, 'GET', successCallback);
        };

        /**
         * put - Perform a put api-call
         *
         * @param  {string} route               the route
         * @param  {Object} data                the request body
         * @param  {Function} successCallback   the function to call on status OK
         * @return {Void}
         */
        this.put = function (route, data, successCallback) {
            return self.request(route, 'PUT', successCallback, data);
        };

        /**
         * post - Perform a post api-call
         *
         * @param  {string} route              the route
         * @param  {Object} data               the request body
         * @param  {Function} successCallback  the function to call on status OK
         * @return {Void}
         */
        this.post = function (route, data, successCallback) {
            return self.request(route, 'POST', successCallback, data);
        };

        /**
         * delete - Perform a delete api-call
         *
         * @param  {string} route               the route
         * @param  {Function} successCallback   method called on OK
         * @return {Void}
         */
        this.delete = function (route, successCallback) {
            return self.request(route, 'DELETE', successCallback, null);
        };
    });
}());
