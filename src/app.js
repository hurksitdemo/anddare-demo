(function () {
    "use strict";
    app
    .config (function ($locationProvider, $urlRouterProvider, $stateProvider) {
        $locationProvider.html5Mode(true);
        $urlRouterProvider.otherwise('/home');

        $stateProvider
        .state("home", {
            url: "/home",
            views: {
                "master@": {
                    controller: "rootController",
                    templateUrl: "src/view/master/master-default.html"
                },
                "content@home": {
                    controller: "homeController",
                    templateUrl: "src/view/home/home.html"
                }
            }
        });
    });
} ());
