(function () {
    "use strict";

    app.controller ("homeController", function ($scope, $httpService) {

        $scope.feedback = [];

        $scope.files = [];

        $scope.selected = {
            file: null
        };

        $scope.host = {
            filename: "index.html",
            host: "",
            port: 21,
            username: "",
            password: ""
        };

        $scope.getProcessedFile = function (fileName) {
            $scope.selected.file = null;

            $scope.appendFeedback ("Loading file...");

            $httpService.get ("api/ftp/processed/" + fileName, function (response) {
                if (response.status === 200) {
                    $scope.selected.file = response.data.data || null;

                    if (response.data != null && response.data.feedback != null && response.data.data != null) {
                        $scope.appendFeedback (response.data.feedback);
                    } else {
                        $scope.appendFeedback ("Could not load file, empty api response.");
                    }
                } else if (response.data != null && response.data.feedback != null) {
                    $scope.appendFeedback (response.data.feedback);
                } else {
                    $scope.appendFeedback ("An error has occured.");
                }
            });
        };

        $scope.listProcessedFiles = function () {
            $httpService.get ("api/ftp/processed", function (response) {
                if (response.status === 200) {
                    if (response.data != null && response.data.feedback != null && response.data.data != null) {
                        $scope.appendFeedback (response.data.feedback);
                        $scope.files = response.data.data;
                    } else {
                        $scope.appendFeedback ("Could not list files, empty api response.");
                    }
                } else if (response.data != null && response.data.feedback != null) {
                    $scope.appendFeedback (response.data.feedback);
                } else {
                    $scope.appendFeedback ("An error has occured.");
                }
            });
        };

        $scope.processFile = function () {
            $httpService.post ("api/ftp/", {
                fileName: $scope.host.filename,
                hostname: $scope.host.host,
                username: $scope.host.username,
                password: $scope.host.password,
                port: $scope.host.port
            }, function (response) {
                if (response.status === 200) {
                    $scope.selected.file = response.data.data || null;

                    if (response.data != null && response.data.feedback != null && response.data.data != null) {
                        $scope.appendFeedback (response.data.feedback);
                        $scope.listProcessedFiles();
                    } else {
                        $scope.appendFeedback ("Could not process file, empty api response.");
                    }
                } else if (response.data != null && response.data.feedback != null) {
                    $scope.appendFeedback (response.data.feedback);
                } else {
                    $scope.appendFeedback ("An error has occured.");
                }
            });
        };

        $scope.appendFeedback = function (message) {
            var currentDate = new Date();

            $scope.feedback.push({
                time: currentDate.getDate() + "/"
                    + (currentDate.getMonth()+1)  + "/"
                    + currentDate.getFullYear() + " @ "
                    + currentDate.getHours() + ":"
                    + currentDate.getMinutes() + ":"
                    + currentDate.getSeconds(),
                content: message
            });
        };

    });
} ());
